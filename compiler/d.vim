if exists("current_compiler")
  finish
endif
let current_compiler = "dmd"

if exists(":CompilerSet") != 2    " older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

CompilerSet makeprg=dmd

CompilerSet errorformat= "%f(%l): Error: %m,%-Z%p^,%-C%.%#,%-G%.%#"
