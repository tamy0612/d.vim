if exists("b:did_ftplugin_d")
  finish
endif
let b:did_ftplugin = 1

setlocal ts=2 sw=2
setlocal cinoptions-=g4
setlocal cinoptions+=g2

setlocal include=\s*import
setlocal includeexpr=substitute(v:fname,'\\.','/','g')
setlocal suffixesadd=.d,.di
if exists('$D_IMPORT_DIR')
  setlocal path=$D_IMPORT_DIR/phobos/,$D_IMPORT_DIR/druntime/import/
elseif exists('$D_HOME')
  setlocal path=$D_HOME/import/
endif
setlocal path+=.,./src/,./source/

setlocal commentstring=//%s
